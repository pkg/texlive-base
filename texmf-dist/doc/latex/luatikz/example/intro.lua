tikz.within( '*' )

local p1 = p{ 1, 1 }
local p2 = p{ 6, 2 }
local l1 = line{ p1, p2 }

draw{ l1 }
draw{ line{ p{ 2, 2 }, p{ 3, 3 } } }
