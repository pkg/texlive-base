tikz.within( '*' )

local l1 = line{ p{ 0, 0 }, p{ 2, 3 } }
local l2 = line{ p1 = p{ 0, 3 }, p2 = p{ 2, 1 } }

draw{ l1 }
draw{ l2 }
