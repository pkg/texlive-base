tikz.within( '*' )

local l1 = line{ p{ 0,  0 }, p{ 3,  1 } }
local l2 = line{ p{ 0, -1 }, p{ 3,  0 } }
local l3 = line{ p{ 0, -2 }, p{ 3, -1 } }

draw{ arrow, l1 }
draw{ double_arrow, dashed, l2 }
draw{ color = purple, line_width = 2, dotted, l3 }

put{ node{
	at = l3.pc,
	anchor = center,
	align = center,
	color = purple,
	rotate = l3.phi * 180 / math.pi,
	text = 'label for purple line',
	text_width = '2cm',
} }
