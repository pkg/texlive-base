tikz.within( '*' )

put{
	node{
		at = p{ 0, 0 },
		text = [[$a^2 + b^2 = c^2$]],
	},
	node{
		at = p{ 2, 1 },
		text = [[$sin(\phi) = \frac{b}{a}$]],
	}
}
