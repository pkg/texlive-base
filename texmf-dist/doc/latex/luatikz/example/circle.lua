tikz.within( '*' )

draw{
	circle{
		at = p{ 0, 0 },
		radius = 2,
	},
	circle{
		at = p{ 2, 1 },
		radius = 1,
	}
}
