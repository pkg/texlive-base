tikz.within( '*' )

local r1 =
	rect{
		pc   = p{ 0, 0 },
		size = p{ 3, 3 },
	}

draw{ r1 }

put{
	node{
		at = r1.pc,
		text = [[this is the center of a rect]],
		text_width = '2cm',
		rotate = 45,
	},
}
