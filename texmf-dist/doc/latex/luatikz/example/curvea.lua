tikz.within( '*' )

local c =
	curve{
		points =
		{
			p{ 0, 0 },
			p{ 2, 2 },
			p{ 4, 0 },
		},
	}

draw{ line_width=1, c }
c.drawHelpers( 'p' )
