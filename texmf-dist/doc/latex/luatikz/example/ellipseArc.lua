tikz.within( '*' )

draw{
	ellipseArc{
		at = p{ 0, 0 },
		from = 0,
		to = 90,
		xradius = 3,
		yradius = 2,
	},
	ellipseArc{
		at = p{ 2, 1 },
		xradius = 1.5,
		yradius = 1,
		from = -45,
		to = 45,
	}
}
