tikz.within( '*' )

local l1 = line{ p{ 0, 0 }, p{ 2, 3 } }
local l2 = line{ p1 = p{ 0, 3 }, p2 = p{ 2, 1 } }
local pi = l1.intersectLine( l2 )

draw{ l1 }
draw{ l2 }
draw{ circle{ at = pi, radius = 0.1 } }
