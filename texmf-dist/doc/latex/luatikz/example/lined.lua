tikz.within( '*' )

local l = line{ p{ 0, 0 }, p{ 5, 0 } }
draw{ l }

for i = 1, 10
do
	l =
		line{
			p1     = l.p2,
			phi    = l.phi + math.pi / 5,
			length = l.length * 2 / 3,
		}
	draw{ l }
end
