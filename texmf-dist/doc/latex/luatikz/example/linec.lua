tikz.within( '*' )

draw{ line{
	p1  = p{ 0, 0 },
	phi = -math.pi / 4,
	length = 2,
} }
