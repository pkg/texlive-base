tikz.within( '*' )

local list = { 1, 1.5, 2, 4, 999 }

for i, v in ipairs( list )
do
	local pi = p{ ( i - 1 ) * 2.5, 0 }
	draw{
		superEllipse{
			at = pi,
			n = v,
			xradius = 1,
			yradius = 1,
		}
	}
	put{
		node{
			at = pi,
			text = 'n = ' .. v,
		}
	}
end
