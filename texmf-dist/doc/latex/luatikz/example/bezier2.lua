tikz.within( '*' )

local bz =
	bezier2{
		p1 = p{ 0, 0 },
		pc = p{ 2, 2 },
		p2 = p{ 4, 0 },
	}

draw{ line_width=1, bz }
bz.drawHelpers( 'p' )
