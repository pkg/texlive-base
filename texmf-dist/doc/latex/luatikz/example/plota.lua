tikz.within( '*' )

draw{
	plot{
		at   = p{ 0, 0 },
		from = 0,
		to   = 5,
		step = 0.05,
		func =
			function( d )
				return p{ d, math.pow( d - 2.5, 2 ) / 2 }
			end
	}
}
