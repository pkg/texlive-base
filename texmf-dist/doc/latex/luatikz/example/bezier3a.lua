tikz.within( '*' )

local bz =
	bezier3{
		p1  = p{ 0, 0 },
		pc1 = p{ 1, 2 },
		pc2 = p{ 3, 3 },
		p2  = p{ 4, 0 },
	}

draw{ line_width=1, bz }
bz.drawHelpers( 'p' )
