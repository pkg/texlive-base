-----------------
Luatikz is a graphics library to draw tikz graphics
using the Lua programming language.
-----------------

License:

LaTeX Project Public License 1.3c
MIT License (expat)
