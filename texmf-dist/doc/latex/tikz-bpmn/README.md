Version: 2025-01-07

This Tikz library provides primitives for drawing Business Process Modelling and Notation (BPMN) models.
It includes tasks, subprocesses, events, task markers and gateways.
The symbols aim to follow the BPMN standard [https://www.omg.org/bpmn/] as closely as possible. 
Please refer to the documentation for further information.

Author: Sander J.J. Leemans, RWTH Aachen University, Germany

This material is subject to the LaTeX Project Public License 1.3c.
